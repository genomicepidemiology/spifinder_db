SPIFinder Database documentation
=============

The database database is a curated database of Salmonella Pathogenetic Island

## Content of the repository
1. SPI.fsa - The Salmonella Pathogenetic Island database
2. INSTALL.py - Script for indexing the database with KMA
3. config - Configuration fie that describes the content of the databases
4. notes.txt - Lists additional notes of the entries in the databases
5. categoryFuncTable.csv - Table explaining the category function numbers
6. README.md

## Installation
Download the SPIFinder database by cloning the database:
```bash
git clone https://git@bitbucket.org/genomicepidemiology/spifinder_db.git
```
The database can be used with BLAST as-is.

If you want to use the database with the stand-alone SPIFinder tool, and wishes
to use the mapping based method, the database needs to be indexed with KMA.

### Installing KMA (optional):
```bash
# Go to the directory in which you want KMA installed
cd /some/path
# Clone KMA
git clone https://bitbucket.org/genomicepidemiology/kma.git
# Go to kma directory and compile code
cd kma && make
```

### Indexing with *INSTALL.py*
If you have KMA installed you either need to have the kma_index in your PATH or
you need to provide the path to kma_index to INSTALL.py

#### a) Run INSTALL.py in interactive mode
```bash
# Go to the database directory
cd path/to/spifinder_db
python3 INSTALL.py
```
If kma_index was found in your path a lot of indexing information will be
printed to your terminal, and will end with the word "done".

If kma_index wasn't found you will recieve the following output:
```bash
KMA index program, kma_index, does not exist or is not executable
Please input path to executable kma_index program or choose one of the options below:
	1. Install KMA using make, index db, then remove KMA.
	2. Exit
```
You can now write the path to kma_index and finish with <enter> or you can
enter "1" or "2" and finish with <enter>.

If "1" is chosen, the script will attempt to install kma in your systems
default temporary location. If the installation is successful it will proceed
to index your database, when finished it will delete the kma installation again.

#### b) Run INSTALL.py in non_interactive mode
```bash
# Go to the database directory
cd path/to/spifinder_db
python3 INSTALL.py /path/to/kma_index non_interactive
```
The path to kma_index can be omitted if it exists in PATH or if the script
should attempt to do an automatic temporary installation of KMA.

#### c) Index database manually (not recommended)
It is possible to index the databases manually, but is generally not recommended
as it is more prone to error. If you choose to do so, be aware of the naming of
the indexed files.

This is an example of how to index the SPIFinder database files:
```bash
# Go to the database directory
cd path/to/spifinder_db
# create indexing directory
mkdir kma_indexing
# Index files using kma_index
kma_index -i SPI.fsa -o kma_indexing/SPI
```

### Documentation

The documentation available as of the date of this release can be found at:
https://bitbucket.org/genomicepidemiology/spifinder_db/overview.


Citation
=======

When using the method please cite:

SPIFinder 
Is the Evolution of Salmonella enterica subsp. enterica Linked to Restriction-Modification Systems?
Roer, L., Hendriksen, R. S., Leekitcharoenphon, P., Lukjancenko, O., Kaas, R. S., Hasman, H., and Aarestrup, F. M.
mSystems 1(3):e00009-16.
https://doi.org/10.1128/mSystems.00009-16

Database:
The SPIs in this database are based on the PAI database:
Yoon SH*, Park YK, and Kim JF. 2015. PAIDB v2.0: exploration and analysis of pathogenicity and resistance islands Nucleic Acids Res. 43:D624-D630 (doi: 10.1093/nar/gku985, Published in advance, October 21, 2014)
It can be accessed here: http://www.paidb.re.kr


License
=======

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.